/*
 * This file will only contain comments for creating a custom thing.h
 */

#ifndef THING_H_
#define THING_H_

#include "som.h"

#define YOUR_THING_NAME	"Environment Monitoring System(EMS)"

#define LEDS_NUMBER		3  /*!< Number of leds used */

/*
 * The following LEDs are used by the Core Embedded Framework for showing notifications during startup.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing these SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * */
#define SYSTEM_HMI_LED_MONO_RED		SOM_PIN_97   /*!< Used by the Core Embedded Framework as RED LED */
#define SYSTEM_HMI_LED_GREEN		SOM_PIN_79   /*!< Used by the Core Embedded Framework as GREEN LED */
#define SYSTEM_HMI_LED_BLUE			SOM_PIN_85   /*!< Used by the Core Embedded Framework as BLUE LED */

/**/
#define LEDS_ACTIVE_STATE 	0
/**/
#define LEDS_LIST { SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE }

#define BUTTONS_NUMBER	1

/*
 * The following BUTTON is used by the Core Embedded Framework as a Factory Reset Button for deleting all saved configurations.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing the SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * */
#define SYSTEM_RESET_BUTTON		SOM_PIN_101

/**/
#define BUTTONS_ACTIVE_STATE	1
/**/
#define BUTTONS_LIST		{ SYSTEM_RESET_BUTTON }

/********************************************** DHT22 GPIO Selection *****************************************************/

#define DHT22_DOUT_GPIO		25


/********************************************** Buzzer GPIO Selection *****************************************************/

#define BUZZER_GPIO		26


/********************************************** Relay Control GPIO Selection *****************************************************/

#define RELAY_CONTROL_GPIO		SOM_PIN_86


/********************************************** LCD GPIOs **********************************************/

#define LCD_D0	15
#define LCD_D1	2
#define LCD_D2	0
#define LCD_D3	4

#define LCD_D4	16
#define LCD_D5	17
#define LCD_D6	5
#define LCD_D7	18

#define LCD_RS      27
#define LCD_RW      14
#define LCD_EN      12
#define LCD_BUSY    LCD_D7


#endif /* THING_H_ */
