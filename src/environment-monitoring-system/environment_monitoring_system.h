/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef ENVIRONMENT_MONITORING_SYSTEM_H_
#define ENVIRONMENT_MONITORING_SYSTEM_H_

#include "thing.h"
#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"

/**
 *
 */
void environment_monitoring_system_init();

/**
 *
 */
void environment_monitoring_system_loop();

#endif	/* ENVIRONMENT_MONITORING_SYSTEM_H_ */
