/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "environment_monitoring_system.h"
#include "dht22.h"
#include "clcd.h"
#include "threading.h"

#include <string.h>

#define TAG	"environment_monitoring_system"

static ThreadHandle dht22_read_task_handle = NULL;

/******************************************** Static Functions *************************************/

void dht22_read_task(void* param) {
	while (1) {
		core_err res = readDHT();

		if (res != CORE_OK) {
			CORE_LOGE(TAG, "Failed to read DHT22");
			errorHandler(res);
		} else {
			CORE_LOGI(TAG, "DHT22 Temperature: %.2f, Humidity: %.2f", getTemperature(), getHumidity());
			char str[20] = { 0 };
			sprintf(str, "Temp:%.2f", getTemperature());
			set_string(str, 0, 0, 0, 15, SCROLL_OFF, 0, 0, OVERWRITE);

			bzero(str, sizeof(str));
			sprintf(str, "Humidity:%.2f", getHumidity());
			set_string(str, 1, 1, 0, 15, SCROLL_OFF, 0, 0, OVERWRITE);
		}
		TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
	}

	TaskDelete(NULL);
}

/****************************************************************************************************/

void environment_monitoring_system_init() {
	CORE_LOGI(TAG, "In your_module_init");

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */

	// set the DHT22 Data out GPIO to the DHT22-lib
	core_err res = setDHTgpio(DHT22_DOUT_GPIO);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "Failed to Configured DHT22 Data GPIO");
	}

	res = init_clcd_lcd(2, 16);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "Failed to Configured CLCD");
	}

	bool status = TaskCreate(dht22_read_task, "dht22_read_task", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_5,
			&dht22_read_task_handle);
	if (status != true) {
		CORE_LOGE(TAG, "Failed to Start DHT22 read Task");
	}

	res = configure_gpio(BUZZER_GPIO, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "Failed to Configured Buzzer Control GPIO");
	}

	res = configure_gpio(RELAY_CONTROL_GPIO, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "Failed to Configured Relay Control GPIO");
	}

	CORE_LOGI(TAG, "Returning from your_module_init");
}

void environment_monitoring_system_loop() {
	//CORE_LOGI(TAG, "In your_module_loop");

}
