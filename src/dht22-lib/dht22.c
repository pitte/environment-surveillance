/*------------------------------------------------------------------------------

 DHT22 temperature & humidity sensor AM2302 (DHT22) driver

 Jun 2017:	Ricardo Timmermann, new for DHT22

 Code Based on Adafruit Industries and Sam Johnston and Coffe & Beer. Please help
 to improve this code.

 This example code is in the Public Domain (or CC0 licensed, at your option.)

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.

 PLEASE KEEP THIS CODE IN LESS THAN 0XFF LINES. EACH LINE MAY CONTAIN ONE BUG !!!

 ---------------------------------------------------------------------------------*/

#include "dht22.h"

#include <stdio.h>
#include "core_logger.h"
#include "gpio_core.h"
#include "system_utils.h"
#include "core_error.h"
#include "delay_utils.h"
#include "gpio_helper_api.h"

#include "thing.h"

// == global defines =============================================

static const char* TAG = "DHT22-lib";

//#if DHT22_DOUT_GPIO
//
//int DHTgpio = DHT22_DOUT_GPIO;				// my default DHT pin = DHT22_DOUT_GPIO
//
//#else

int DHTgpio = -1;

//#endif

float humidity = 0.;
float temperature = 0.;

// == set the DHT used pin=========================================

core_err setDHTgpio(int dout_gpio) {
	DHTgpio = dout_gpio;
	core_err ret = configure_gpio(DHTgpio, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		DHTgpio = -1;
	}
	return ret;
}

// == get temp & hum =============================================

float getHumidity() {
	return humidity;
}
float getTemperature() {
	return temperature;
}

// == error handler ===============================================

void errorHandler(int response) {
	switch (response) {

	case CORE_ERR_TIMEOUT:
		CORE_LOGE(TAG, "DHT22 Data Read Timeout\n");
		break;

	case CORE_ERR_INVALID_CRC:
		CORE_LOGE(TAG, "DHT22 Data CheckSum error\n");
		break;

	case CORE_ERR_INVALID_ARG:
		CORE_LOGE(TAG, "DHT22 Data-out GPIO Parameter Error\n");
		break;

	case CORE_OK:
		break;

	default:
		CORE_LOGE(TAG, "DHT22 Unknown error\n");
	}
}

/*-------------------------------------------------------------------------------
 ;
 ;	get next state
 ;
 ;	I don't like this logic. It needs some interrupt blocking / priority
 ;	to ensure it runs in realtime.
 ;
 ;--------------------------------------------------------------------------------*/

int getSignalLevel(int usTimeOut, bool state) {

	int uSec = 0;
	while ( gpio_io_get_level(DHTgpio) == state) {

		if (uSec > usTimeOut)
			return -1;

		++uSec;
		core_us_delay(1);		// uSec delay
	}

	return uSec;
}

/*----------------------------------------------------------------------------
 ;
 ;	read DHT22 sensor

 copy/paste from AM2302/DHT22 Docu:

 DATA: Hum = 16 bits, Temp = 16 Bits, check-sum = 8 Bits

 Example: MCU has received 40 bits data from AM2302 as
 0000 0010 1000 1100 0000 0001 0101 1111 1110 1110
 16 bits RH data + 16 bits T data + check sum

 1) we convert 16 bits RH data from binary system to decimal system, 0000 0010 1000 1100 → 652
 Binary system Decimal system: RH=652/10=65.2%RH

 2) we convert 16 bits T data from binary system to decimal system, 0000 0001 0101 1111 → 351
 Binary system Decimal system: T=351/10=35.1°C

 When highest bit of temperature is 1, it means the temperature is below 0 degree Celsius.
 Example: 1000 0000 0110 0101, T= minus 10.1°C: 16 bits T data

 3) Check Sum=0000 0010+1000 1100+0000 0001+0101 1111=1110 1110 Check-sum=the last 8 bits of Sum=11101110

 Signal & Timings:

 The interval of whole process must be beyond 2 seconds.

 To request data from DHT:

 1) Sent low pulse for > 1~10 ms (MILI SEC)
 2) Sent high pulse for > 20~40 us (Micros).
 3) When DHT detects the start signal, it will pull low the bus 80us as response signal,
 then the DHT pulls up 80us for preparation to send data.
 4) When DHT is sending data to MCU, every bit's transmission begin with low-voltage-level that last 50us,
 the following high-voltage-level signal's length decide the bit is "1" or "0".
 0: 26~28 us
 1: 70 us

 ;----------------------------------------------------------------------------*/

#define MAXdhtData 5	// to complete 40 = 5*8 Bits

int readDHT() {
	if (DHTgpio != -1) {
		int uSec = 0;

		uint8_t dhtData[MAXdhtData];
		uint8_t byteInx = 0;
		uint8_t bitInx = 7;

		for (int k = 0; k < MAXdhtData; k++)
			dhtData[k] = 0;

		// == Send start signal to DHT sensor ===========

		gpio_io_set_direction(DHTgpio, GPIO_IO_MODE_OUTPUT);

		// pull down for 3 ms for a smooth and nice wake up
		gpio_set_level(DHTgpio, 0);
		core_us_delay(3000);

		// pull up for 25 us for a gentile asking for data
		gpio_io_set_level(DHTgpio, 1);
		core_us_delay(25);

		gpio_io_set_direction(DHTgpio, GPIO_IO_MODE_INPUT);		// change to input mode

		// == DHT will keep the line low for 80 us and then high for 80us ====

		uSec = getSignalLevel(85, 0);
//	CORE_LOGI( TAG, "Response = %d", uSec );
		if (uSec < 0)
			return CORE_ERR_TIMEOUT;

		// -- 80us up ------------------------

		uSec = getSignalLevel(85, 1);
//	CORE_LOGI( TAG, "Response = %d", uSec );
		if (uSec < 0)
			return CORE_ERR_TIMEOUT;

		// == No errors, read the 40 data bits ================

		for (int k = 0; k < 40; k++) {

			// -- starts new data transmission with >50us low signal

			uSec = getSignalLevel(56, 0);
			if (uSec < 0)
				return CORE_ERR_TIMEOUT;

			// -- check to see if after >70us rx data is a 0 or a 1

			uSec = getSignalLevel(75, 1);
			if (uSec < 0)
				return CORE_ERR_TIMEOUT;

			// add the current read to the output data
			// since all dhtData array where set to 0 at the start,
			// only look for "1" (>28us us)

			if (uSec > 40) {
				dhtData[byteInx] |= (1 << bitInx);
			}

			// index to next byte

			if (bitInx == 0) {
				bitInx = 7;
				++byteInx;
			} else
				bitInx--;
		}

		// == get humidity from Data[0] and Data[1] ==========================

		humidity = dhtData[0];
		humidity *= 0x100;					// >> 8
		humidity += dhtData[1];
		humidity /= 10;						// get the decimal

		// == get temp from Data[2] and Data[3]

		temperature = dhtData[2] & 0x7F;
		temperature *= 0x100;				// >> 8
		temperature += dhtData[3];
		temperature /= 10;

		if (dhtData[2] & 0x80) 			// negative temp, brrr it's freezing
			temperature *= -1;

		// == verify if checksum is ok ===========================================
		// Checksum is the sum of Data 8 bits masked out 0xFF

		if (dhtData[4] == ((dhtData[0] + dhtData[1] + dhtData[2] + dhtData[3]) & 0xFF))
			return CORE_OK;

		else
			return CORE_ERR_INVALID_CRC;
	} else {
		return CORE_ERR_INVALID_ARG;
	}
}

