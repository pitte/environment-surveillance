/* 

	DHT22 temperature sensor driver

*/

#ifndef DHT22_H_  
#define DHT22_H_

#include <stdbool.h>
#include "core_error.h"

#ifdef __cplusplus
extern "C" {
#endif


// == function prototypes =======================================

/**
 * @brief	Configure DHT22 Data-out GPIO
 *
 * @param[in]	dout_gpio	DHT22 Data-out GPIO
 *
 * @return
 * 		- CORE_OK on Success
 * 		- CORE_ERR_INVALID_ARG DHT22 Data-out GPIO Parameter error
 */
core_err setDHTgpio(int dout_gpio);

/**
 * @brief	Execute DHT22
 *
 * @return
 *
 */
void 	errorHandler(core_err response);

/**
 * @brief	Execute DHT22 Data read process
 *
 * @return
 * 		- CORE_OK on Success
 * 		- CORE_ERR_TIMEOUT	Read Operation timed out
 * 		- CORE_ERR_INVALID_CRC	CRC or checksum was invalid
 * 		- CORE_ERR_INVALID_ARG DHT22 Data-out GPIO Parameter isn't configured
 */
core_err 	readDHT();

/**
 * @brief	Get DHT22 Humidity value
 */
float 	getHumidity();

/**
 * @brief	Get DHT22 Temperature value
 */
float 	getTemperature();

/**
 *
 */
int 	getSignalLevel( int usTimeOut, bool state );


#ifdef __cplusplus
}
#endif

#endif
